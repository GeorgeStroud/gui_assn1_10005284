const { app, BrowserWindow } = require('electron')
const path = require('path')
const url = require('url')

let win

function createWindow() {
  win = new BrowserWindow({ width: 800, height: 600 })
  win.loadURL(url.format({
    pathname: path.join(__dirname, 'index.html'),
    protocol: 'file:',
    slashes: true
  }))

  //win.webContents.openDevTools()

  win.on('closed', () => {
    win = null
  })
}

app.on('ready', createWindow)

app.on('window-all-closed', () => {
  if (process.platform !== 'darwin') {
    app.quit()
  }
})

app.on('activate', () => {
  if (win === null) {
    createWindow()
  }
})

//This function allows the sliders to change the background color 
function changeColor() {

  var a = document.getElementById("r");
  var b = document.getElementById("g");
  var c = document.getElementById("b");
  document.body.style.backgroundColor = 'rgb(' + parseInt(a.value) + ',' + parseInt(b.value) + ',' + parseInt(c.value) + ')';

  document.getElementById('rdisplay').value = a.value;
  document.getElementById('gdisplay').value = b.value;
  document.getElementById('bdisplay').value = c.value;

  }

  // This function changes the background color based on what the text boxes say 
  function textchange(){
  var d = document.getElementById("rdisplay");
  var e = document.getElementById("gdisplay");
  var f = document.getElementById("bdisplay");
  document.body.style.backgroundColor = 'rgb(' + parseInt(d.value) + ',' + parseInt(e.value) + ',' + parseInt(f.value) + ')';
   document.getElementById('r').value = d.value;
  document.getElementById('g').value = e.value;
  document.getElementById('b').value = f.value;
  }

  function reseter() {
  document.body.style.backgroundColor ='white'
    location.reload();    
  }